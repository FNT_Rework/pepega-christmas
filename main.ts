import { Color, EventsSDK, GameRules, GUIInfo, Menu, Rectangle, RendererSDK, Vector2 } from "wrapper/Imports"

const menu = Menu.AddEntry("Pepega")
menu.IsHidden = true
const save = menu.AddToggle("show", false)
save.IsHidden = true

const optionImage = {
	name: 1,
	isHidden: true,
	timeShowEnded: 0,
	awaitConfigTime: Date.now() + 5 * 1000,
}

function GetPanel(pos: Vector2): Vector2 {
	return new Vector2(GUIInfo.ScaleWidth(pos.x), GUIInfo.ScaleHeight(pos.y))
}

function Text(text: string, position: Rectangle, color = Color.White, divide = 0) {
	const textSize = divide === 0 ? position.y / 1.3 : position.y / divide
	const pos = Vector2.FromVector3(RendererSDK.GetTextSize(text, RendererSDK.DefaultFontName, textSize))
		.MultiplyScalarForThis(-1)
		.AddScalarX(position.pos2.x)
		.AddScalarY(position.pos2.y)
		.DivideScalarForThis(2)
		.AddScalarX(position.pos1.x)
		.AddScalarY(position.pos1.y)
		.RoundForThis()
	RendererSDK.Text(text, pos, color, RendererSDK.DefaultFontName, textSize)
}

EventsSDK.on("Draw", () => {
	if (save.value || GameRules?.IsInGame)
		return
	const Size = GetPanel(new Vector2(800, 600))
	const Position = RendererSDK.WindowSize.DivideScalar(100)
	const wPos = GetPanel(new Vector2(
		Position.x + (Size.x / 2) + 100,
		Position.y + (Size.y / 2) - 100,
	))
	if (Date.now() > optionImage.awaitConfigTime)
		optionImage.isHidden = false
	if (optionImage.isHidden)
		return
	if (optionImage.timeShowEnded === 0)
		optionImage.timeShowEnded = Date.now() + 15 * 1000
	if (Date.now() > optionImage.timeShowEnded) {
		save.value = true
		return
	}
	RendererSDK.Image(`gitlab.com/FNT_Rework/pepega-christmas/scripts_files/${optionImage.name}.png`, wPos, -1, Size, Color.White)
	const recPos = new Rectangle(wPos, Size)
	recPos.pos1.AddScalarY(recPos.pos2.y / 2)
	const year = new Date().getFullYear()

	const text = year === 2021
		? `Happy 2022 year!\nThank you for being with us!`
		: `Happy 2022 year!!!\nThank you for being with us!`

	Text(Menu.Localization.Localize(text), recPos, Color.White, 10)
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	[`Happy 2022 year!\nThank you for being with us!`, `С наступающим 2022 годом!\nСпасибо что Вы с нами!`],
	[`Happy 2022 year!!!\nThank you for being with us!`, `С наступившим 2022 годом!!!\nСпасибо что Вы с нами!`],
]))
